from django.apps import AppConfig


class OcrsConfig(AppConfig):
    name = 'ocrs'
