from django.contrib import admin
from django.urls import path,include
from . import views

urlpatterns = [
   path('',views.rendrer, name="try"),
   path('login',views.login, name='login'),
   path('fir',views.fir, name='fir')
]
