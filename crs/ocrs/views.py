from django.shortcuts import render
from django.http import HttpResponse
from .forms import userForm, loginForm, firForm
from .models import Users, Case

user = Users()
case = Case()

def rendrer(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = userForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            user.email = form.cleaned_data['email']
            user.name = form.cleaned_data['name']
            user.type = form.cleaned_data['type']
            user.password = form.cleaned_data['password']
            user.address = form.cleaned_data['address']
            user.phone = form.cleaned_data['phone']
            user.save()
            return render(request, 'sign_up-success.html', {'name': user.name})

    # if a GET (or any other method) we'll create a blank form
    else:
        form = userForm()

    return render(request, 'index.html', {'form': form})

def login(request):

    if request.method == 'POST':
        login_form = loginForm(request.POST)

        if login_form.is_valid():
            entered_email = login_form.cleaned_data['login_email']
            db_user = Users.objects.get(email=entered_email)
            actual_password = db_user.password
            entered_password = login_form.cleaned_data['login_password']
            print ('actual password is: - ', actual_password)
            print ('entered password is: - ', entered_password)
            if actual_password == entered_password:
                if db_user.type == 'Complainant':
                    return render(request,'complainant_log.html',{'data': db_user})
                elif db_user.type == 'Officer':
                    return render(request, 'officer_log.html', {'data': db_user})
                elif db_user.type == 'Forensic':
                    return render(request, 'forensics_log.html', {'data': db_user})
            else:
                return render(request, 'login.html', {'login_form': login_form, 'message':'INVALID CREDENTIALS! Please try again !! Ganbarimas !!!'})

    else:
        login_form = loginForm()

    return render(request, 'login.html', {'login_form': login_form})

def fir(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        fir_form  = firForm(request.POST)

        # check whether it's valid:
        if fir_form.is_valid():
            case.complainant_name = fir_form.cleaned_data['c_name']
            case.complainant_fa_or_hu = fir_form.cleaned_data['c_fa_or_hu']
            case.complainant_address = fir_form.cleaned_data['c_address']
            case.complainant_prof = fir_form.cleaned_data['c_prof']
            case.complainant_email = fir_form.cleaned_data['c_email']
            case.complainant_phone = fir_form.cleaned_data['c_phone']
            case.complainant_dob = fir_form.cleaned_data['c_dob']
            case.complainant_nation = fir_form.cleaned_data['c_nation']
            case.complainant_aadhar = fir_form.cleaned_data['c_aadhar']
            case.complainant_passport = fir_form.cleaned_data['c_passport']
            case.complainant_dl = fir_form.cleaned_data['c_dl']
            case.location = fir_form.cleaned_data['location']
            case.crime_type = fir_form.cleaned_data['c_type']
            case.near_pstation = fir_form.cleaned_data['near_pstation']
            case.save()
            data1 = Case.objects.get(complainant_name=fir_form.cleaned_data['c_name'])
            return render(request, 'fir_success.html', {'data1': data1})

    # if a GET (or any other method) we'll create a blank form
    else:
        fir_form = firForm()

    return render(request, 'firform.html', {'fir_form': fir_form})

