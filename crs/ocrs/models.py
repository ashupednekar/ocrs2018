from django.db import models

# Create your models here
class Users(models.Model):
    email = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    address = models.CharField(max_length=500)
    type = models.CharField(max_length=100)
    phone = models.CharField(max_length=50)

class Case(models.Model):
    complainant_name = models.CharField(max_length=200)
    complainant_fa_or_hu= models.CharField(max_length=300)
    complainant_address = models.CharField(max_length=200)
    complainant_prof = models.CharField(max_length=200)
    complainant_email = models.CharField(max_length=200)
    complainant_phone = models.CharField(max_length=200)
    complainant_dob = models.CharField(max_length=200)
    complainant_nation = models.CharField(max_length=200)
    complainant_aadhar = models.CharField(max_length=200)
    complainant_passport = models.CharField(max_length=200)
    complainant_dl = models.CharField(max_length=200)
    crime_type = models.CharField(max_length=200)
    location = models.CharField(max_length=200)
    near_pstation = models.CharField(max_length=200)
