from django import forms

class userForm(forms.Form):
    email = forms.CharField(widget=forms.EmailInput(attrs={'class':'form-control','placeholder':"What's your email address?"}))
    name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'What is your name?'}))
    type = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'What kind of a user you are? (Complainant | Officer | Forensics)'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Please enter your password.'}))
    address = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Where did the crime occur?'}))
    phone = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Please enter your phone number?'}))

class loginForm(forms.Form):
    login_email = forms.CharField(widget=forms.EmailInput(attrs={'class':'form-control','placeholder':"What's your email address?"}))
    login_password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Please enter your password.'}))

class firForm(forms.Form):
    c_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'What is your name?'}))
    c_fa_or_hu = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':"What is your father's or husband's name?"}))
    c_address = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Where do ypu live?'}))
    c_prof = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'What do you do for a living?'}))
    c_email = forms.CharField(widget=forms.EmailInput(attrs={'class':'form-control','placeholder':'What is your email id?'}))
    c_phone = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'What is your contact number?'}))
    c_dob = forms.CharField(widget=forms.DateInput(attrs={'class':'form-control','placeholder':'When is your birthday?'}))
    c_nation = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'What is your nationality?'}))
    c_aadhar = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'What is your aadhar ID?'}))
    c_passport = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'What is your passport number?'}))
    c_dl = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'What is your dl number?'}))
    location = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Where did the crime occur?'}))
    c_type = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'What category would you classify the crime?'}))
    near_pstation = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'What is the name of your closest police station?'}))
