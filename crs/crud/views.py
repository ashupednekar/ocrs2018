from django.shortcuts import render
from django.http import HttpResponse
from .forms import userForm, loginForm, firForm
from ocrs.models import Users, Case

user = Users()
case = Case()

def rendrer(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = userForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            user.email = form.cleaned_data['email']
            user.name = form.cleaned_data['name']
            user.type = form.cleaned_data['type']
            user.password = form.cleaned_data['password']
            user.address = form.cleaned_data['address']
            user.phone = form.cleaned_data['phone']
            user.save()
            return render(request, 'crudio.html', {'message': "User successfully Created."})

    # if a GET (or any other method) we'll create a blank form
    else:
        form = userForm()

    return render(request, 'crudio.html', {'form': form})

def list(request):
    query = Users.objects.all()
    return render(request, 'user-list.html',{'query':query})

def case_list(request):
    query = Case.objects.all()
    return render(request, 'case-list.html',{'query':query})

def fir_db(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = firForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            case.complainant_name = form.cleaned_data['c_name']
            case.complainant_fa_or_hu = form.cleaned_data['c_fa_or_hu']
            case.complainant_address = form.cleaned_data['c_address']
            case.complainant_prof = form.cleaned_data['c_prof']
            case.complainant_email = form.cleaned_data['c_email']
            case.complainant_phone = form.cleaned_data['c_phone']
            case.complainant_dob = form.cleaned_data['c_dob']
            case.complainant_nation = form.cleaned_data['c_nation']
            case.complainant_aadhar = form.cleaned_data['c_aadhar']
            case.complainant_passport = form.cleaned_data['c_passport']
            case.complainant_dl = form.cleaned_data['c_dl']
            case.crime_type = form.cleaned_data['location']
            case.location = form.cleaned_data['c_type']
            case.near_pstation = form.cleaned_data['near_pstation']
            case.save()
            return render(request, 'crudio.html', {'message': "Case successfully Created."})

    # if a GET (or any other method) we'll create a blank form
    else:
        form = firForm()

    return render(request, 'crudio.html', {'form': form})

