from django.contrib import admin
from django.urls import path,include
from . import views

urlpatterns = [
   path('/sign-up',views.rendrer, name="crud"),
   path('/user-list',views.list, name="crud"),
   path('/fir-db',views.fir_db, name="crud"),
   path('/case-list',views.case_list, name="crud")]
